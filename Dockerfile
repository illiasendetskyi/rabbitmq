FROM node:carbon

LABEL maintainer="Illia Sendetskyi <illiasendetskyi@gmail.com>"

RUN mkdir /app

WORKDIR /app

COPY . .
RUN npm install

EXPOSE 5672

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.2.1/wait /wait
RUN chmod +x /wait

CMD /wait ./script/script.js