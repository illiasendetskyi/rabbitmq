const amqp = require('amqplib');

const calcFib = require('../calcFib/fastFib.js');
const basename = require('path').basename;
const RABBITMQ_URI = process.env.RABBITMQ_URI;
const key = process.argv[2];

if (!key) {
    console.log('Usage next pattern: %s <some.key>',
        basename(process.argv[1]));
    process.exit(1);
}

amqp.connect(RABBITMQ_URI).then((conn) => {
    return conn.createChannel();
}).then((ch) => {
    const ex = 'topic_fibonacci';
    return ch.assertExchange(ex, 'topic', { durable: false })
    .then(() => {
        return ch.assertQueue();
    })
    .then((q) => {
        const queue = q.queue;
        ch.bindQueue(queue, ex, key)
        return queue;
    })
    .then((queue) => {
        return ch.consume(queue, (msg) => {
            const n = parseInt(msg.content.toString());
            console.log(calcFib(n));
        }, { noAck: true });
    })
    .then(() => {
        console.log(` [***] Waiting for messages from '${key}' routing key. To exit press CTRL+C. [***]`);
    });
}).catch((e) => { console.log(e); });