const slowFib = (n) => {
    if (n == 0 || n == 1)
        return n;
    else
        return slowFib(n - 1) + slowFib(n - 2);
};

module.exports = slowFib;