const amqp = require('amqplib');

const RABBITMQ_URI = process.env.RABBITMQ_URI;
amqp.connect(RABBITMQ_URI).then((conn) => {
    console.log(RABBITMQ_URI);
    return conn.createChannel().then((ch) => {
        const ex = 'topic_fibonacci';
        const msg = process.argv[2];
        const key = process.argv[3];
        const ok = ch.assertExchange(ex, 'topic', {durable: false});
        return ok.then(() => {
            ch.publish(ex, key, new Buffer(msg));
            return ch.close();
        });        
    }).finally(() => { conn.close(); });
}).catch((e) => { console.log(e); });